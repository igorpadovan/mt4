<?php
namespace App\View;

class View
{
  public function render($template = '', $content = '') {

  	if ( !empty($template) ) {
  		$template = __DIR__ . DIRECTORY_SEPARATOR . 'Html' . DIRECTORY_SEPARATOR .$template;
  	}
    include_once('Html/default.php');
  }
}