<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" href="public/css/bootstrap.min.css">

    <title>Senha Segura</title>
  </head>
  <body>
    

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="index.php">SENHA SEGURA</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php?controller=controle_dispositivos&action=index">Controle de Dispositivos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?controller=integração_ssh&action=index">Integração SSH</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?controller=criptogafia&action=index">Criptografia</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.php?controller=comparacao_hashes&action=index">Comparação de Hashes</a>
          </li>  
          <li class="nav-item">
            <a class="nav-link" href="index.php?controller=relatorio&action=index">Relatório</a>
          </li>                
        </ul>
      </div>
    </nav>

    
        
    <?php

    if (file_exists($template)) {
        include_once $template;        
    }
    ?>
      

        
    </main>
  </body>
</html>