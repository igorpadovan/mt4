<?php
namespace App\Controller;
use App\View\View;

class Controller
{
	public function index()
  	{
	    $view = new View;
		$view->render();
  	}
}