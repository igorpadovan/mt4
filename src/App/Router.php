<?php
namespace App;

use App\Controller\Controller;

class Router
{

	private $controller;
	private $action;
	private $id;

	public function __construct() {
		if (isset($_GET['controller'])) {
			$this->controller = $_GET['controller'];
		}
		if (isset($_GET['action'])) {
			$this->action = $_GET['action'];
		}
		if (isset($_GET['id'])) {
			$this->action = $_GET['id'];
		}
	}

	public function dispatch() {

		$controller = new Controller();
		$controller->index();
	}
}